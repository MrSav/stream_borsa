package stream_borsa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {

	public List<String> filterAndCalculate(String filePath) {
		
		List<String> filteredList = new ArrayList<>();
		String[] splittedList = null;
		float totalPurchases = 0;
		float totalSales = 0;
		int totalOperations = 0;

		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
			
			filteredList = stream 
					.filter(line -> {
						if(line.matches("^\\w+\\s\\d+\\s\\d+[.,]?\\d*\\s[AV]$"))
							return true;
						else
							return false;
					})
					.collect(Collectors.toList());
			
			for (String element : filteredList) {
				splittedList = element.split(" ");
				
				float numberOfShares = Float.parseFloat(splittedList[1]);
				float numberOfSales = Float.parseFloat(splittedList[2]);
				
				if(splittedList[3].equals("A")) {
					totalPurchases += numberOfShares * numberOfSales;
				} else if(splittedList[3].equals("V")) {
					totalSales += numberOfShares * numberOfSales;
				}
				totalOperations++;
			}
	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		filteredList.forEach(System.out::println);
		System.out.println("Totale operazioni: " + totalOperations);
		System.out.println("Totale acquisti: " + totalPurchases);
		System.out.println("Totale vendite: " + totalSales);

		return filteredList;
	}
}
