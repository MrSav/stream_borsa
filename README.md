This project is a laboratory exercise.
The goal is to create a method that takes a file as input, and through a stream, filters it by taking only the valid lines and then processes it by calculating the product between the number of shares and the price of the same.
Finally, the method must return a list that contains the total number of transactions, the total value of the purchases and the total value of the sales.

Inside the 'res' folder of the project, there is the file to be processed, insert it in the path you prefer.
The path set in the 'TestReadFile' class is "C: \", so if you put the file in a different path, you will need to change the 'filterAndCalculate (String filePath)' function parameter.